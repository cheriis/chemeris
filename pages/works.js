import { Container, Heading, SimpleGrid } from '@chakra-ui/react'
import Layout from '../components/layouts/article'
import Section from '../components/section'
import { WorkGridItem } from '../components/grid-item'

import thumbLetoile from '../public/images/works/letoile.png'
import thumbMIR from '../public/images/works/mir.png'
import thumbSltech from '../public/images/works/sltech.png'
import thumbTT from '../public/images/works/tt.png'

const Works = () => (
  <Layout title="Works">
    <Container>
      <Heading as="h3" fontSize={20} mb={4}>
        Works
      </Heading>

      <SimpleGrid columns={[1, 1, 2]} gap={6}>
        <Section>
          <WorkGridItem id="letoile" title="Letoile" thumbnail={thumbLetoile}>
            The biggest player on the Russian online retail market with unique
            online shop and marketplace
          </WorkGridItem>
        </Section>
        <Section delay={0.1}>
          <WorkGridItem
            id="instoremarket"
            title="Instore.market"
            thumbnail={thumbSltech}
          >
            Aggregator of insurance products
          </WorkGridItem>
        </Section>
        <Section>
          <WorkGridItem id="mir" title="MIR" thumbnail={thumbMIR}>
            Business support and promotion system
          </WorkGridItem>
        </Section>
        <Section delay={0.1}>
          <WorkGridItem id="tendertech" title="Tendertech" thumbnail={thumbTT}>
            Tendertech is a tender&apos;s aggregator-platform
          </WorkGridItem>
        </Section>
      </SimpleGrid>
    </Container>
  </Layout>
)

export default Works
export { getServerSideProps } from '../components/chakra'
