import { Container, Heading } from '@chakra-ui/react'
import Layout from '../components/layouts/article'

const Posts = () => (
  <Layout title="Posts">
    <Container>
      <Heading as="h3" fontSize={20} mb={4}>
        Posts TBD
      </Heading>

      {/*<Section delay={0.1}>*/}
      {/*  <SimpleGrid columns={[1, 2, 2]} gap={6}>*/}
      {/*    <GridItem*/}
      {/*      title=""*/}
      {/*      thumbnail=""*/}
      {/*      href=""*/}
      {/*    />*/}
      {/*  </SimpleGrid>*/}
      {/*</Section>*/}
    </Container>
  </Layout>
)

export default Posts
export { getServerSideProps } from '../components/chakra'
