import { Badge, Container, Link, List, ListItem } from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Meta, Title, WorkImage } from '../../components/work'
import P from '../../components/paragraph'
import Layout from '../../components/layouts/article'

const Work = () => (
  <Layout title="Letoile">
    <Container>
      <Title>
        Letoile <Badge>2021 to present</Badge>
      </Title>
      <P>
        Letoile - The biggest player on the Russian online retail market. In a
        position of Lead Frontent team + certified Scrum knowledge my key
        responsibilities include:
      </P>
      <List my={2}>
        <ListItem ml={4} my={1}>
          - Migrating the frontend legacy code (knockout.js) to vue2. Designing
          and implementing a solution for the gradual tech redesign and
          coexistence of the legacy library knockout and vue like application
          core. This solution saved business money and speed up the usability of
          the site for users.
        </ListItem>
        <ListItem ml={4} my={1}>
          - Designing and creating JavaScript based components
        </ListItem>
        <ListItem ml={4} my={1}>
          - Formating of a backlog of technical features for 2+ years
        </ListItem>
        <ListItem ml={4} my={1}>
          - Formating frontend Scrum team
        </ListItem>
        <ListItem ml={4} my={1}>
          - Interviewing of developers
        </ListItem>
        <ListItem ml={4} my={1}>
          - Implementating of agile processes in the team
        </ListItem>
        <ListItem ml={4} my={1}>
          - Facilitating of scrum events
        </ListItem>
        <ListItem ml={4} my={1}>
          - Maintenance of meetups frontend guild
        </ListItem>
      </List>

      <List ml={4} my={4}>
        <ListItem>
          <Meta>Website</Meta>
          <Link href="https://www.letu.ru" isExternal>
            https://www.letu.ru
            <ExternalLinkIcon mx="2px" />
          </Link>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>JS(ES6), NodeJS, Vue, Vuex, Webpack/vite, Scss</span>
        </ListItem>
        <ListItem>
          <Meta>Services</Meta>
          <span>
            Online shop, Short-links microservice, SSR, Email service, Markdown
            editor
          </span>
        </ListItem>
      </List>

      <WorkImage src="/images/works/letoile.png" alt="Letoile" />
      <WorkImage src="/images/works/letoile2.png" alt="Letoile" />
    </Container>
  </Layout>
)

export default Work
export { getServerSideProps } from '../../components/chakra'
