import { Badge, Container, Link, List, ListItem } from '@chakra-ui/react'
import Layout from '../../components/layouts/article'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Meta, Title, WorkImage } from '../../components/work'
import P from '../../components/paragraph'

const Work = () => (
  <Layout title="Instore.market">
    <Container>
      <Title>
        Instore.market <Badge>2016-2020</Badge>
      </Title>
      <P>
        SL Tech (Instore.market) is a large insurance broker in Russia. In a
        position of Senior Frontend Developer my key responsibilities include:
      </P>
      <List my={2}>
        <ListItem ml={4} my={1}>
          - Creating customisable widget constructor for a Single Page
          Application offering business products which resulted in rapid growth
          of partners and agents network and growing exposure of our business
          solutions to wider client audiences
        </ListItem>
        <ListItem ml={4} my={1}>
          - Creating inner admin interfaces with statistics for partners/agents
        </ListItem>
        <ListItem ml={4} my={1}>
          - Implementing own UI framework improving user experience and
          applications speeds using React and Vue frameworks
        </ListItem>
        <ListItem ml={4} my={1}>
          - Implementing own UI framework improving user experience and
          applications speeds using React and Vue frameworks
        </ListItem>
        <ListItem ml={4} my={1}>
          - Designing and creating JavaScript based components
        </ListItem>
        <ListItem ml={4}>
          - Designing, building of the projects architecture.
        </ListItem>
      </List>

      <List ml={4} my={4}>
        <ListItem>
          <Meta>Website</Meta>
          <Link href="https://instore.market" isExternal>
            https://instore.market
            <ExternalLinkIcon mx="2px" />
          </Link>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>
            JS(ES6), TS, NodeJS, Nuxt(Vue), Vuex, React, Redux, Scss, Webpack
          </span>
        </ListItem>
        <ListItem>
          <Meta>Services</Meta>
          <Link href="https://instore.market/osago" isExternal>
            https://instore.market/osago
            <ExternalLinkIcon mx="2px" />
          </Link>{' '}
          <br />
          <Link href="https://instore.travel" isExternal>
            https://instore.travel
            <ExternalLinkIcon mx="2px" />
          </Link>{' '}
          <br />
          <Link href="https://instore.partners" isExternal>
            https://instore.partners
            <ExternalLinkIcon mx="2px" />
          </Link>
        </ListItem>
      </List>

      <WorkImage src="/images/works/sltech.png" alt="Instore.market" />
      <WorkImage src="/images/works/sltech1.png" alt="Instore.market" />
      <WorkImage src="/images/works/sltech2.png" alt="Instore.market" />
    </Container>
  </Layout>
)

export default Work
export { getServerSideProps } from '../../components/chakra'
