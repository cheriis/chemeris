import {
  Badge,
  Container,
  Link,
  List,
  ListItem,
  SimpleGrid
} from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Meta, Title, WorkImage } from '../../components/work'
import P from '../../components/paragraph'
import Layout from '../../components/layouts/article'

const Work = () => (
  <Layout title="MIR">
    <Container>
      <Title>
        MIR<Badge>2021</Badge>
      </Title>
      <P>Business support and promotion multilanguage system</P>
      <List my={2}>
        <ListItem ml={4}>
          - Designing and creating JavaScript based components
        </ListItem>
        <ListItem ml={4}>
          - Designing, building of the projects architecture.
        </ListItem>
      </List>

      <List ml={4} my={4}>
        <ListItem>
          <Meta>Website</Meta>
          <Link href="https://madeinrussia.ru/" isExternal>
            https://madeinrussia.ru/
            <ExternalLinkIcon mx="2px" />
          </Link>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>
            JS(ES6), TS, NodeJS, Next (React), React hooks, Webpack, Style
            modules
          </span>
        </ListItem>
      </List>

      <WorkImage src="/images/works/mir.png" alt="MIR" />
      <SimpleGrid columns={2} gap={2}>
        <WorkImage src="/images/works/mir1.png" alt="MIR" />
        <WorkImage src="/images/works/mir2.png" alt="MIR" />
      </SimpleGrid>
    </Container>
  </Layout>
)

export default Work
export { getServerSideProps } from '../../components/chakra'
