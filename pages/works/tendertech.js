import { Badge, Container, Link, List, ListItem } from '@chakra-ui/react'
import Layout from '../../components/layouts/article'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Meta, Title, WorkImage } from '../../components/work'
import P from '../../components/paragraph'

const Work = () => (
  <Layout title="Tendertech">
    <Container>
      <Title>
        Tendertech<Badge>2020-2021</Badge>
      </Title>
      <P>
        Tendertech is a tender&apos;s insurance aggregator-platform. In a
        position of Senior Frontend Developer my key responsibilities include:
      </P>
      <List my={2}>
        <ListItem ml={4}>
          - Designing and creating JavaScript based components
        </ListItem>
        <ListItem ml={4}>
          - Designing, building of the projects architecture.
        </ListItem>
        <ListItem ml={4}>
          - Creating inner admin interfaces with statistics for partners/agents
        </ListItem>
      </List>

      <List ml={4} my={4}>
        <ListItem>
          <Meta>Website</Meta>
          <Link href="https://tendertech.ru/" isExternal>
            https://tendertech.ru
            <ExternalLinkIcon mx="2px" />
          </Link>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>JS(ES6), TS, NodeJS, Nuxt(Vue), Vuex, Scss, Webpack</span>
        </ListItem>
      </List>

      <WorkImage src="/images/works/tt.png" alt="TT" />
      <WorkImage src="/images/works/tt2.png" alt="TT" />
      <WorkImage src="/images/works/tt1.png" alt="TT" />
    </Container>
  </Layout>
)

export default Work
export { getServerSideProps } from '../../components/chakra'
