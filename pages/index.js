/* eslint-disable */
import NextLink from 'next/link'
import Link from 'next/link'
import {
  Box,
  Button,
  chakra,
  Container,
  Heading,
  List,
  ListItem,
  useColorModeValue
} from '@chakra-ui/react'
import { ChevronRightIcon } from '@chakra-ui/icons'
import Paragraph from '../components/paragraph'
import Layout from '../components/layouts/article'
import Section from '../components/section'
import Image from 'next/image'
import { BioSection, BioYear } from '../components/bio'
import {
  IoLogoCodepen,
  IoLogoGitlab,
  IoLogoGoogle,
  IoLogoInstagram,
  IoLogoLinkedin
} from 'react-icons/io5'

const ProfileImage = chakra(Image, {
  shouldForwardProp: prop => ['width', 'height', 'src', 'alt'].includes(prop)
})

const Home = () => (
  <Layout>
    <Container>
      <Box
        borderRadius="lg"
        mb={6}
        p={3}
        textAlign="center"
        bg={useColorModeValue('whiteAlpha.500', 'whiteAlpha.200')}
        css={{ backdropFilter: 'blur(10px)' }}
      >
        {/*Hello, I&apos;m JavaScript developer with a big heart!*/}
        Whats up buddy!
      </Box>

      <Box display={{ md: 'flex' }}>
        <Box flexGrow={1}>
          <Heading as="h2" variant="page-title">
            Sergey Chemeris
          </Heading>
          <p>Digital Craftsman ( Developer / Painter )</p>
        </Box>
        <Box
          flexShrink={0}
          mt={{ base: 4, md: 0 }}
          ml={{ md: 6 }}
          textAlign="center"
        >
          <Box
            borderColor="whiteAlpha.800"
            borderWidth={2}
            borderStyle="solid"
            w="100px"
            h="100px"
            display="inline-block"
            borderRadius="full"
            overflow="hidden"
          >
            <ProfileImage
              src="/images/chemeris_profile.png"
              alt="Profile image"
              borderRadius="full"
              width="100%"
              height="100%"
            />
          </Box>
        </Box>
      </Box>

      <Section delay={0.1}>
        <Heading as="h3" variant="section-title">
          Work
        </Heading>
        <Paragraph>
          Hello, I&apos;m Front End Developer with 9+ years of experience coding
          web applications for commercial online services using modern
          JavaScript (ES5, ES6), HTML, and CSS within a diverse job portfolio.
          My technology stack is predominantly web-based, particularly utilizing
          JavaScript frameworks like Vue/Nuxt + Vuex and React/Next + Redux.
        </Paragraph>
        <Paragraph>
          My strategy is to create great, stable, high performance, secure, and
          easy to use applications, which effectively solve business tasks and
          attract wider audiences. Being in love with structure and order I
          particularly enjoy spending time on optimizing web apps and polishing
          them to perfection.
        </Paragraph>
        <Box align="center" my={4}>
          <NextLink href="/works" passHref scroll={false}>
            <Button rightIcon={<ChevronRightIcon />} colorScheme="teal">
              My portfolio
            </Button>
          </NextLink>
        </Box>
      </Section>

      <Section delay={0.2}>
        <Heading as="h3" variant="section-title">
          Bio
        </Heading>
        <BioSection my={2}>
          <BioYear>2021 to present</BioYear>
          Worked at Letoile as a Frontend Team lead
        </BioSection>
        <BioSection my={2}>
          <BioYear>2021</BioYear>
          Worked at MIR as a Senior Frontend Developer
        </BioSection>
        <BioSection my={2}>
          <BioYear>2020</BioYear>
          Worked at Tendertech as a Senior Frontend Developer
        </BioSection>
        <BioSection my={2}>
          <BioYear>2016</BioYear>
          Worked at Sl Tech (Instore.market) as a Senior Frontend Developer
        </BioSection>
        <BioSection my={2}>
          <BioYear>2014</BioYear>
          Completed the Master&apos;s Program of Informatics and computer
          engineering at Moscow Aviation Institute (National Research
          University)
        </BioSection>
      </Section>

      <Section delay={0.3}>
        <Heading as="h3" variant="section-title">
          I ♥
        </Heading>
        <Paragraph>
          Art, Music, Learning, Books, Sport, Photography, Computer games
        </Paragraph>
      </Section>

      <Section delay={0.3}>
        <Heading as="h3" variant="section-title">
          On the web
        </Heading>
        <List>
          <ListItem>
            <Link
              href="mailto:chemeris.sergey@gmail.com&body=Привет Сергей!"
              target="_blank"
            >
              <Button
                variant="ghost"
                colorScheme="teal"
                leftIcon={<IoLogoGoogle />}
              >
                chemeris.sergey@gmail.com
              </Button>
            </Link>
          </ListItem>
          <ListItem>
            <Link
              href="https://www.linkedin.com/in/sergey-chemeris"
              target="_blank"
            >
              <Button
                variant="ghost"
                colorScheme="teal"
                leftIcon={<IoLogoLinkedin />}
              >
                sergey-chemeris
              </Button>
            </Link>
          </ListItem>
          <ListItem>
            <Link href="https://gitlab.com/cheriis" target="_blank">
              <Button
                variant="ghost"
                colorScheme="teal"
                leftIcon={<IoLogoGitlab />}
              >
                cheriis
              </Button>
            </Link>
          </ListItem>
          <ListItem>
            <Link href="https://www.instagram.com/chemerisris" target="_blank">
              <Button
                variant="ghost"
                colorScheme="teal"
                leftIcon={<IoLogoInstagram />}
              >
                @chemerisris
              </Button>
            </Link>
          </ListItem>
          <ListItem>
            <Link href="https://codepen.io/cheris" target="_blank">
              <Button
                variant="ghost"
                colorScheme="teal"
                leftIcon={<IoLogoCodepen />}
              >
                @cheris
              </Button>
            </Link>
          </ListItem>
        </List>
      </Section>
    </Container>
  </Layout>
)

export default Home
export { getServerSideProps } from '../components/chakra'
