import Link from 'next/link'
import { Text, useColorModeValue } from '@chakra-ui/react'
import LuckyIcon from './icons/luckyicon'
import styled from '@emotion/styled'

const LogoBox = styled.span`
  font-weight: bold;
  font-size: 18px;
  display: inline-flex;
  align-items: center;
  padding: 0 10px;
  height: 100%;
  line-height: 20px;

  > svg {
    transition: 200ms ease;
    margin-right: 0.5rem;
  }

  &:hover > svg {
    transform: rotate(20deg);
  }
`

const Logo = () => {
  return (
    <Link href="/" scroll={false}>
      <a>
        <LogoBox>
          <LuckyIcon />
          <Text
            color={useColorModeValue('gray.800', 'whiteAlpha.900')}
            fontFamily='M PLUS Rounded 1c", sans-serif'
            fontWeight="bold"
            ml={3}
          >
            Sergey Chemeris
          </Text>
        </LogoBox>
      </a>
    </Link>
  )
}

export default Logo
